use crate::config::Config;
use futures_util::stream::StreamExt;
use lazy_static::lazy_static;
use log::{error, info};
use prometheus::*;
use std::time::Duration;
use tokio::time::delay_for;

lazy_static! {
    static ref PING_HISTOGRAM: HistogramVec = register_histogram_vec!(
        "ping_rtt_milliseconds",
        "The ping round trip time in milliseconds",
        &["target"],
        vec![
            0.5, 1.0, 5.0, 10.0, 15.0, 20.0, 25.0, 50.0, 75.0, 100.0, 150.0, 200.0, 250.0, 300.0,
            350.0, 400.0, 450.0, 500.0, 550.0, 600.0, 650.0, 700.0, 750.0, 800.0, 900.0, 1000.0,
            1250.0, 1500.0, 1750.0, 2000.0
        ]
    )
    .unwrap();
}

pub(crate) async fn start_pinging_hosts(
    config: Config,
) -> std::result::Result<(), tokio_ping::Error> {
    let pinger = tokio_ping::Pinger::new().await?;
    for (host, interval) in config.hosts.clone() {
        info!("Spawn ping task for {}", host);
        let pingchain = pinger.chain(host).timeout(Duration::from_secs(3));
        let host = host.to_string();
        tokio::spawn(pingchain.stream().for_each(move |ping_result| {
            match ping_result {
                Ok(time) => match time {
                    Some(time) => {
                        PING_HISTOGRAM
                            .with_label_values(&[&host])
                            .observe(time.as_millis() as f64);
                    }
                    None => PING_HISTOGRAM.with_label_values(&[&host]).observe(3000.0),
                },
                Err(error) => error!("Couldn't ping {}: {}", &host, error),
            }
            delay_for(Duration::from_millis(interval))
        }));
    }
    Ok(())
}
