use log::error;

mod config;
mod metrics;
mod ping;
use crate::config::{read_config, setup_clap, setup_fern};
use crate::metrics::start_serving_metrics;
use crate::ping::start_pinging_hosts;

#[tokio::main]
async fn main() -> Result<(), ()> {
    let clap = setup_clap();
    setup_fern(clap.occurrences_of("v"));
    let config = match read_config(clap.value_of("config").unwrap()) {
        Ok(config) => config,
        Err(_) => {
            error!("Couldn't read config file!");
            return Err(());
        }
    };

    tokio::spawn(start_pinging_hosts(config.clone()));
    start_serving_metrics(config.clone()).await;
    Ok(())
}
